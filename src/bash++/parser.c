/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           parser.c                                                **
 ** @brief          Function definitions for parsing bash++ to bash         **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include "parser.h"

#include <iso646.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bash++.h"


/** @brief          Parses bash++ style variable assignments.
 *
 *  @details        Takes variable assignments in the form var = "val" and
 *                  outputs them in the form var="val".  Function assumes all
 *                  buffers have been allocated/provide sufficient space.
 *  @param in_line  The line of text from the bash++ input being parsed.
 *  @param o_line   The resulting bash command after parsing.
 *  @return         An integer status value 0 if successfull or 1 if failed.
 */
int
parse_assign (ProgramData data, const char *in_line, char *match, char *o_line)
{
    memcpy (o_line, in_line, match - in_line);
    strcat (o_line, "=");
    strcat (o_line, match + 3);
    if (data->verbose) 
        printf ("Parsed input line:\n\t%sand wrote out\n\t%s", in_line, o_line);
    return 0;
}


/** @brief          Parses bash++ style if-then statements.
 *
 *  @details        Takes an if statement with a then command on the same line
 *                  and adds a semicolon after the conditional, before the
 *                  then command.
 *  @param in_line  The line of text from the bash++ input being parsed.
 *  @param o_line   The resulting bash command after parsing.
 *  @return         An integer status value 0 if successfull or 1 if failed.
 */
int
parse_if (ProgramData data, const char *in_line, char *match, char *o_line)
{
    memcpy (o_line, in_line, match - in_line);
    strcat (o_line, ";");
    strcat (o_line, match);
    if (data->verbose) 
        printf ("Parsed input line:\n\t%sand wrote out\n\t%s", in_line, o_line);
    return 0;
}


/** @brief          Takes a line from the input file and parses it to be
 *                  written to the output file.
 *  @param in_line  The line read from the input file
 *  @param o_line   The line that was parsed and is ready to be written to the
 *                  output file.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         0 if the name was successfully parsed, or 1 if it wasn't.
 */
int
parse_line (ProgramData data, const char *in_line, char *o_line)
{
    /* Set to true (1) while we're parsing a repeat loop */
    static int in_loop = 0;
    char *match;
    if (in_loop) {
        match = strstr (in_line, "}");
        if (NULL not_eq match) {
            strcpy (o_line, in_line);
            strcat (o_line, "done");
            in_loop = 0;
            return 0;
        }
    }
    match = strstr (in_line, " = ");
    if (NULL not_eq match)
        return parse_assign (data, in_line, match, o_line);
    match = strstr (in_line, " then");
    if (NULL not_eq match)
        return parse_if (data, in_line, match, o_line);
    match = strstr (in_line, "repeat");
    if (NULL not_eq match) {
        in_loop = 1;
        return parse_repeat (data, in_line, o_line);
    }
    strcpy (o_line, in_line);
    return 0;
}


/** @brief          Parses bash++ style repeat loops.
 *
 *  @details        Takes a bash++ style @code repeat $num times @endcode
 *                  and outputs a bash for loop in the form
 *                  @code for (i=0; i<$num; ++i); do @endcode
 *  @param in_line  The line of text from the bash++ input being parsed.
 *  @param o_line   The resulting bash command after parsing.
 *  @return         An integer status value 0 if successfull or 1 if failed.
 */
int
parse_repeat (ProgramData data, const char *in_line, char *o_line)
{
    char *tokens[5] = { NULL };
    char *cpy = malloc (strlen (in_line));
    strcpy (cpy, in_line);
    tokens[0] = strtok (cpy, " ");
    int i = 0;
    while (NULL not_eq tokens[i]) {
        ++i;
        tokens[i] = strtok (NULL, " ");
    }
    strcpy (o_line, "for (( i=0; i<");
    strcat (o_line, tokens[1]);
    strcat (o_line, "; ++i )); do");
    if (data->verbose) 
        printf ("Parsed input line:\n\t%sand wrote out\n\t%s", in_line, o_line);
    return 0;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
