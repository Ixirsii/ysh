/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           parser.h                                                **
 ** @brief          File input/output functions for bash++ major assignment **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef BASHPP_PARSER_H
#define BASHPP_PARSER_H


#include "bash++.h"


int         parse_assign        (ProgramData data, const char *in_line,
        char *match, char *o_line);
int         parse_if            (ProgramData data, const char *in_line,
        char *match, char *o_line);
int         parse_line          (ProgramData data, const char *in_line,
        char *o_line);
int         parse_repeat        (ProgramData data, const char *in_line,
        char *o_line);


#endif /* BASHPP_PARSER_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
