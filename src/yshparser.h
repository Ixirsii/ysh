/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           parser.h                                                **
 ** @brief          File input/output functions for bash++ major assignment **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef YSH_PARSER_H
#define YSH_PARSER_H


#include "ysh.h"


int         str_count           (const char * const str,
                                 const char * const delim);
int         str_split           (char *str, const char* const delim,
                                 char **tokens);


#endif /* YSH_PARSER_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/

