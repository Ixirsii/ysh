/** @file           ysh.h
 *  @brief          Y Shell public function prototypes
 *  @author         Ryan Porterfield
 *  @author         Juan Hernandez
 *  @author         Noel Behailu
 *  @author         Erika Gutierrez
 *  @copyright      BSD 3 Clause (See COPYING file)
 *  @version        1.3.0:20150611
 */


#ifndef YSH_H
#define YSH_H

#include <stdbool.h>

#define YSH_BUILD_DATE      "2015-06-11"
#define YSH_BUILD_STATUS    "Development"
#define YSH_COPYRIGHT       "Copyright (C) 2014 CSCE 3600-001 Group 21"
#define YSH_DISCLAIMER      "This is free software; see the source for " \
                            "copying conditions.  There is NO\nwarranty; not " \
                            "even for MERCHANTABILITY or FITNESS FOR A " \
                            "PARTICULAR PURPOSE"
#define YSH_VERSION         "1.3.0"

typedef struct {
    char **argv;
    int argc;
} Command;

/** @struct         ProgramData ysh.h "src/ysh.h"
 *  @brief          Stores basic program data used by most functions.
 *
 */
typedef struct ProgramData_s {
    bool        running;    /**< Set to 1 (true) while the shell is running */
    char        *prompt;    /**< The string to "prompt" the user for input */
    const char  *exec;      /**< The name of the executable */
    const int   help;       /**< Does the program usage need to be printed */
    const int   version;    /**< Does the program version need to be printed */
} *ProgramData;


#endif /* YSH_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
