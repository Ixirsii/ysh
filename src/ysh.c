/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** TODO:           Comments                                                **
 **                                                                         **
 ** @file           ysh.h                                                   **
 ** @brief          Y Shell function definitions                            **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include "ysh.h"

#include <fcntl.h>
#include <iso646.h>
#include <limits.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "commands.h"
#include "yshparser.h"

#define HELP_BIT            (1 << 0)
#define VERBOSE_BIT         (1 << 1)
#define VERSION_BIT         (1 << 2)


const char*  help[]         =   { "-h", "--help" };
const char*  version[]      =   { "-V", "--version" };


void            delete_command      (Command* cmd);
void            exec_cmd            (const Command* cmd);
Command*        get_command         ();
void            handle_signal       (int signo);
ProgramData     init_data           (const char* exec, int flags);
Command*        new_command         ();
int             parse_args          (int argc, const char** argv);
void            print_help          (ProgramData data);
int             print_info          (ProgramData data);
void            print_version       (ProgramData data);
void            readline            (char* line);
void            run                 (ProgramData data);

/** @brief          Safely deletes a Command that was initialized with the
 *                  @link new_command @endlink function.
 *
 *  @param cmd      A pointer to a Command structure that is being deleted.
 */
void
delete_command (Command* cmd)
{
    free (cmd->full_cmd);
    free (cmd->cmd);
    free (cmd->argv);
    free (cmd);
}


/** @brief
 *
 */
void
exec_cmd (const Command *cmd)
{
    if (fork () == 0) {
        int i = system (cmd->full_cmd);
        if (i < 0) {
            printf ("ysh: command not found: %s\n", cmd->cmd);
            _exit (EXIT_FAILURE);
        }
        _exit (EXIT_SUCCESS);
    } else {
        wait (NULL);
    }
}


/** @brief          Reads a line in from stdin and creates and returns a
 *                  Command based on it.
 *
 *  @return         A new Command pointer created and initialized from
 *                  @link new_command @endlink
 */
Command*
get_command ()
{
    char cmd_str[FILENAME_MAX];
    memset (cmd_str, 0, FILENAME_MAX);
    readline (cmd_str);
    return new_command (cmd_str);
}


/** @brief          Exits the program if an interrupt signal is caught.
 *
 *  @details        If an interrupt signal (C-c) is thrown by the user, the
 *                  function catches and handles it by exiting the program.
 *  @param signo    The signal number.
 */
void
handle_signal(int signo)
{
    printf("\nCaught deadly signal %d, exiting\n", signo);
    fflush(stdout);
    exit (SCHAR_MAX);
}


/** @brief          Initializes a ProgramData struct for the program.
 *
 *  @details        Uses the flags passed to create a ProgramData structure
 *                  containing all relevant program data for the running
 *                  instance of the program.
 *  @param exec     The name of the executable
 *  @param i_name   The name of the input bash++ file
 *  @param flags    An integer whose bits are set based on command line
 *                  arguments passed at run time.
 *  @return         A new dynamically allocated instance of ProgramData
 */
ProgramData
init_data (const char *exec, int flags)
{
    #define YSH_DEFAULT_PROMPT "ysh> "
    ProgramData data = malloc (sizeof (struct ProgramData_s));
    /* Trick to initialize constant struct members */
    struct ProgramData_s init = {
        .running    = true,
        .prompt     = malloc (strlen (YSH_DEFAULT_PROMPT)),
        .exec       = exec,
        .help       = (flags & HELP_BIT) ? 1 : 0,
        .version    = (flags & VERSION_BIT) ? 1 : 0,
    };
    strncpy (init.prompt, YSH_DEFAULT_PROMPT, strlen (YSH_DEFAULT_PROMPT));
    memcpy (data, &init, sizeof (*data));
    return data;
}


/** @brief          Create and initialize a new Command structure.
 *
 *  @param cmd_str  A string command, probably read from stdin.
 *  @return         A dynamically allocated Command structure.
 */
Command*
new_command (const char* const cmd_str)
{
    #define DELIM " "
    Command *cmd = malloc (sizeof (Command));
    int arg_count = str_count (cmd_str, DELIM) + 1;
    int cmd_len = strlen (cmd_str) + 1;

    cmd->full_cmd = malloc (cmd_len);
    cmd->cmd = malloc (cmd_len);
    cmd->argv = malloc (arg_count * sizeof (char*));

    strncpy (cmd->full_cmd, cmd_str, cmd_len);
    strncpy (cmd->cmd, cmd_str, cmd_len);
    str_split (cmd->cmd, DELIM, cmd->argv);
    return cmd;
}


/** @brief          Parses command line arguments and sets internal flags
 *                  accordingly
 *
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @return         An integer variable with bits set according to flags
 *                  passed. Effectively a boolean array.
 */
int
parse_args (int argc, const char **argv)
{
    int flags = 0;
    for (int i = 1; i < argc; ++i) {
        const char *arg = argv[i];
        if (not strcmp (arg, help[0]) or not strcmp (arg, help[1]))
            flags |= HELP_BIT;
        else if (not strcmp (arg, version[0]) or not strcmp (arg, version[1]))
            flags |= VERSION_BIT;
    }
    return flags;
}


/** @brief          Print program usage and flag information.
 *
 *  @details        Print's program usage information when program called with
 *                  -h or --help. Presumably the program exits after this but
 *                  the function behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_help (ProgramData data)
{
    printf ("Usage: %s [OPTION] <file>\n", data->exec);
    printf ("Parses a bash++ file and outputs a bash file.\n\n");
    printf ("  <file>\t\tthe bash++ file being converted to a bash file\n");
    printf ("  %s, %s\t\tshow this help message and exit\n", help[0],
            help[1]);
    printf ("  %s, %s\t\tshow program's version number and exit\n",
            version[0], version[1]);
}


/** @brief          Checks if help or version needs to be printed.
 *
 *  @details        If the program was called with (-h, --help) or 
 *                  (-v, --version) the appropriate print function is called
 *                  and the function returns true (1).
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         true (1) if -h, --help, -v, or --version was called or
 *                  false (0) if not.
 */
int
print_info (ProgramData data)
{
    if (data->help) {
        print_help (data);
        return 1;
    } else if (data->version) {
        print_version (data);
        return 1;
    }
    return 0;
}


/** @brief          Print program version number, copyright, and disclaimer.
 *
 *  @details        Print's program's version number, copyright line, and
 *                  disclaimer when program is called with -v or --version
 *                  Presumably the program exits after this but the function
 *                  behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_version (ProgramData data)
{
    printf ("%s %s %s (%s)\n", data->exec, YSH_VERSION, YSH_BUILD_DATE,
            YSH_BUILD_STATUS);
    printf ("%s\n%s\n", YSH_COPYRIGHT, YSH_DISCLAIMER);
}


/** @brief
 *
 */
void
readline (char *line)
{
    char c;
    int index = 0;
    while ('\n' not_eq (c = getchar ()))
        line[index++] = c;
}


/** @brief
 *
 */
void
run (ProgramData data)
{
    Command clr = {.argv = NULL, .cmd = NULL, .full_cmd = "/usr/bin/clear"};
    exec_cmd (&clr);
    while (data->running) {
        Command* cmd;
        printf ("%s", data->prompt);
        cmd = get_command ();
        if (is_ysh_command (cmd)) {
            ysh_command_func cmd_func = get_ysh_command (cmd);
            cmd_func (data, cmd);
        } else {
            exec_cmd (cmd);
        }
        delete_command (cmd);
    }
}


/** @brief          Called on program execution.
 *
 *  @details        Initializes and runs program.
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @return         Status of program upon completion. EXIT_SUCCESS (0) if the
 *                  program exited successfully, otherwise some error code will
 *                  be returned.
 */
int
main (int argc, const char const **argv)
{
    int flags = parse_args (argc, argv);
    ProgramData data = init_data (argv[0], flags);
    signal (SIGINT, handle_signal);
    if (data->help) {
        print_help (data);
        return EXIT_SUCCESS;
    } else if (data->version) {
        print_version (data);
        return EXIT_SUCCESS;
    }

    run (data);
    return EXIT_SUCCESS;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
