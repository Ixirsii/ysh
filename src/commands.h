/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           commands.h                                              **
 ** @brief          ysh built-in functions and commmand execution functions **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef YSH_COMMANDS_H
#define YSH_COMMANDS_H

#include <stdbool.h>
#include "ysh.h"

typedef int (*ysh_command_func) (ProgramData data, Command *cmd);

int                 add_alias           (Command* cmd); 
ysh_command_func    get_ysh_command     (Command* cmd);
bool                is_ysh_command      (Command* cmd);
void                print_alias         (Command* cmd);
int                 remove_alias        (Command* cmd);

int                 ysh_alias           (ProgramData data, Command* cmd);
int                 ysh_bg              (ProgramData data, Command* cmd);
int                 ysh_fg              (ProgramData data, Command* cmd);
int                 ysh_echo            (ProgramData data, Command* cmd);
int                 ysh_exit            (ProgramData data, Command* cmd);
int                 ysh_which           (ProgramData data, Command* cmd);

#endif /* YSH_COMMANDS_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
