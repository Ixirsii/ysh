/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           parser.c                                                **
 ** @brief          Function definitions for parsing bash++ to bash         **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include "yshparser.h"
#include <stdlib.h>
#include <string.h>


/** @brief          Count the number of occurences of delim in str.
 *
 *  @details        Copies the input string then uses strtok to split the string
 *                  into tokens using the list of delimiters and returns the    
 *                  count of tokens.
 *  @param str      The string being split into tokens.
 *  @param delim    A string list of delimiter characters used to split the
 *                  input string.
 *  @return         The number of tokens the string could be split into.
 */
int
str_count (const char * const str, const char * const delim)
{
    char cpy[strlen (str)];
    strcpy (cpy, str);
    int count = 1;
    char *token = strtok (cpy, delim);
    /* 
     * token is intentionally being assigned via strtok. The loop will stop
     * when token is NULL when strtok reaches the end of the string
     */
    while ((token = strtok (NULL, delim)))
        ++count;
    return count;
}


/** @brief          Splits the string into an array of strings.
 *
 *  @details        Uses strtok to split the string into tokens using the list
 *                  of delimiters and returns the count of tokens.
 *  @param str      The string being split into tokens.
 *  @param delim    A string list of delimiter characters used to split the
 *                  input string.
 *  @param tokens   The array of strings after being split
 *  @return         The number of tokens the string was split into.
 */
int
str_split (char *str, const char * const delim, char **tokens)
{
    int count = str_count (str, delim) + 1;
    tokens[0] = strtok (str, delim);
    for (int i = 1; i < count; ++i)
        tokens[i] = strtok (NULL, delim);
    return count;
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
