/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           commands.h                                              **
 ** @brief          ysh built-in functions and commmand execution functions **
 **                                                                         **
 ** @author         Ryan Porterfield                                        **
 ** @author         Juan Hernandez                                          **
 ** @author         Noel Behailu                                            **
 ** @author         Erika Gutierrez                                         **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/

#include "commands.h"
#include <iso646.h>
#include <stdlib.h>
#include <string.h>

/* **************************** Type Definitions **************************** */

/** @struct         YSHCommand commands.h "src/commands.h"
 *  @brief          A struct providing a string-command pairing for ysh
 *                  built-in functions.
 */
struct YSHCommand {
    /**
     * The case sensitive string command name
     */
    const char* const       cmd;
    /** 
     * The @link ysh_command_func @endlink that gets executed when the command
     * is used.
     */
    const ysh_command_func  func;
};


/* *************************** Constant Variables *************************** */

/** @brief          ysh echo command which takes an input string and prints it
 *                  out to stdout.
 */
static struct YSHCommand const ECHO_COMMAND = {
    .cmd = "echo",
    .func = ysh_echo
};


/** @brief          ysh exit command which takes an optional integer and exits
 *                  EXIT_SUCCESS (0) by default or the integer argument.
 */
static struct YSHCommand const EXIT_COMMAND = {
    .cmd = "exit",
    .func = ysh_exit,
};


/** @brief          List of all @link YSH_COMMAND @endlink that is NULL
 *                  terminated for easy iteration.
 */
static const struct YSHCommand* const YSH_COMMANDS[] = {
    &ECHO_COMMAND, &EXIT_COMMAND, NULL,
};


/* ******************************* Functions ******************************** */

int
ysh_echo (ProgramData data, Command *cmd)
{
    return EXIT_SUCCESS;
}


/** @brief          Exit ysh.
 *
 *  @TODO           Exit with supplied exit code
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @param cmd      The @link Command @endlink form of the command the user
 *                  executed.
 *  @return         EXIT_SUCCESS (0) if the command was executed successfully,
 *                  otherwise EXIT_FAILURE (1).
 */
int
ysh_exit (ProgramData data, Command *cmd)
{
    data->running = false;
    return EXIT_SUCCESS;
}


/** @brief          Returns the @link ysh_command_func @endlink for the
 *                  command.
 *
 *  @details        Iterates through the list of @link YSH_COMMANDS @endlink
 *                  and returns the corresponding
 *                  @link ysh_command_func @endlink if a match is found,
 *                  otherwise returns NULL.
 *  @param cmd      The @link Command @endlink form of the command the user
 *                  executed.
 *  @return         @link ysh_command_func @endlink if a match is found,
 *                  otherwise NULL.
 */
ysh_command_func
get_ysh_command (Command *cmd)
{
    for (int i = 0; YSH_COMMANDS[i]; ++i) {
        if (not strcmp (YSH_COMMANDS[i]->cmd, cmd->cmd))
            return YSH_COMMANDS[i]->func;
    }
    return NULL;
}


/** @brief          Returns true if the command is a ysh built-in command,
 *                  otherwise returns false.
 *
 *  @param cmd      The @link Command @endlink form of the command the user
 *                  executed.
 *  @return         true (1) if the command is a ysh built-in command,
 *                  otherwise returns false (0).
 */
bool
is_ysh_command (Command *cmd)
{
    for (int i = 0; YSH_COMMANDS[i]; ++i) {
        if (not strcmp (YSH_COMMANDS[i]->cmd, cmd->cmd))
            return true;
    }
    return false;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
