YSH
===

Authors:

* Juan Hernandez <JuanHernandez12@my.unt.edu>
* Erika Gutierrez <ErikaCristinaGutierrezPortillo@my.unt.edu>
* Noel Behailu <NoelBehailu@my.unt.edu>
* Ryan Porterfield <RyanPorterfield@my.unt.edu>

Date:       2014-11-19  
Status:     Development
Version:    1.3.0  


About
-----
Y SHell (YSH) is a simple shell implementation written in C.  The base shell
code was provided, then thoroughly modified.  The current version of YSH looks
nothing like the original.


TODO
----
For a more detailed TODO list see the TODO file

* Implement YSH built-in function echo
* Implement I/O redirection
* Implement Pipes
* Implement the ability to execute programs in the background
* Implement YSH built-in function alias
* Implement YSH built-in function which


Usage
-----
To compile run `make`  
To run type `./ysh`  


License
-------
Copyright (c) Ryan Porterfield 2014  
All rights reserved.  
  
Redistribution and use in source and binary forms, with or without  
modification, are permitted provided that the following conditions  
are met:  

1.  Redistributions of source code must retain the above copyright  
    notice, this list of conditions and the following disclaimer.  
2.  Redistributions in binary form must reproduce the above copyright  
    notice, this list of conditions and the following disclaimer in the   
    documentation and/or other materials provided with the distribution.  
3.  Neither the name of the copyright holder nor the names of its  
    contributors may be used to endorse or promote products derived from  
    this software without specific prior written permission.  

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A  
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR  
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS  
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
