CC=gcc
CFLAGS=-std=c99 -O2 -Wall -Wextra -pedantic
OFLAGS=-c $(CFLAGS)
BIN=bin
OUT=ysh

all: command main parser
	$(CC) $(CFLAGS) $(BIN)/commands.o $(BIN)/ysh.o $(BIN)/yshparser.o -o $(OUT)

clean:
	@rm -rf $(BIN)
	@rm -rf $(OUT)

init:
	@mkdir -p bin

command:
	$(CC) $(OFLAGS) src/commands.c -o $(BIN)/commands.o

main: init
	$(CC) $(OFLAGS) src/ysh.c -o $(BIN)/ysh.o

parser:
	$(CC) $(OFLAGS) src/yshparser.c -o $(BIN)/yshparser.o
